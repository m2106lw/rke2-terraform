resource "tls_private_key" "sshkey" {
    algorithm   = "ECDSA"
  	ecdsa_curve = "P384"
}

resource "local_file" "public_key" {
	content         = tls_private_key.sshkey.public_key_openssh
	filename        = "${path.module}/id_rke2.pub"
	file_permission = "0744"
}

resource "local_file" "private_key" {
	content         = tls_private_key.sshkey.private_key_pem
	filename        = "${path.module}/id_rke2"
	file_permission = "0600"
}

resource "random_password" "rke2_token" {
  length           = 64
  special          = true
  override_special = "_%@#%&()-_=+[]{}<>?"
}

resource "digitalocean_ssh_key" "rke2_sshkey" {
    name       = "rke2-key"
    public_key = tls_private_key.sshkey.public_key_openssh
}

resource "digitalocean_droplet" "rke2" {
	for_each           = {first = 1, second = 2, third = 3}
    image              = "ubuntu-18-04-x64"
    name               = "test-rke${each.value}"
    region             = "sfo3"
    size               = "s-4vcpu-8gb"
    ipv6               = true
    private_networking = true
	ssh_keys           = [digitalocean_ssh_key.rke2_sshkey.fingerprint]
}

resource "digitalocean_floating_ip" "ips" {
	for_each   = {for key, value in digitalocean_droplet.rke2 : value.name => digitalocean_droplet.rke2[key]}
    droplet_id = each.value.id
    region     = each.value.region
}

resource "digitalocean_loadbalancer" "public" {
  name   = "rke2-lb"
  region = "sfo3"

  forwarding_rule {
    entry_port     = 9345
    entry_protocol = "https"
	tls_passthrough = true
    target_port     = 9345
    target_protocol = "https"
  }

  forwarding_rule {
    entry_port     = 6443
    entry_protocol = "https"
	tls_passthrough = true
    target_port     = 6443
    target_protocol = "https"
  }

  healthcheck {
    port     = 9345
    protocol = "tcp"
  }

  droplet_ids =  [for key, value in digitalocean_droplet.rke2 : value.id]
}

data "digitalocean_domain" "domain" {
	name = var.domain
}

resource "digitalocean_record" "record" {
	domain   = data.digitalocean_domain.domain.name
	type     = "A"
	name     = "rke2"
	value    = digitalocean_loadbalancer.public.ip //each.value
}

resource "local_file" "rke2_extra_vars_server" {
	sensitive_content = templatefile("${path.module}/templates/extra_vars.yml.tpl", {
		cluster_url  = digitalocean_record.record.fqdn
    	server_setup = true
		token        = random_password.rke2_token.result
	})
	filename = "${path.module}/ansible/server_extra_vars.yml"
}

resource "local_file" "ansible_inventory" {
	sensitive_content = templatefile("${path.module}/templates/inventory.tpl", {
		vms    = digitalocean_droplet.rke2
		ips    = digitalocean_floating_ip.ips
		sshkey = local_file.private_key.filename
	})
	filename = "${path.module}/ansible/inventory"
}

/*
resource "null_resource" "install_rke2" {
	for_each = {(digitalocean_droplet.rke2.name) = digitalocean_floating_ip.ips.ip_address}
#  # Changes to any instance of the cluster requires re-provisioning
#  triggers = {
#    cluster_instance_ids = join(",", aws_instance.cluster.*.id)
#  }

  # Bootstrap script can run on any instance of the cluster
  # So we just choose the first in this case
  connection {
    host        = each.key
    type        = "ssh"
    user        = "root"
    private_key = tls_private_key.sshkey.private_key_pem
  }

  provisioner "remote-exec" {
    # Bootstrap script called with private_ip of each node in the clutser
    inline = [
      "curl -sfL https://get.rke2.io | sh -",
	  "systemctl enable rke2-server.service",
	  "systemctl start rke2-server.service"
    ]
  }
}
*/