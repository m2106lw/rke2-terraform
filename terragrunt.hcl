locals {
	creds = yamldecode(file("./credentials.yml"))

	access_key      = local.creds.access_key
	secret_key      = local.creds.secret_key
	spaces_bucket   = local.creds.spaces_bucket
	spaces_endpoint = local.creds.spaces_endpoint
	token 			= local.creds.token
}

terraform {
	extra_arguments "auto_approve" {
    	commands  = ["apply", "destroy"]
    	arguments = ["-auto-approve"]
  	}
}

# Configure Terragrunt to automatically store tfstate files in an S3 bucket
# I don't care about locking in this instance, but would be important elsewhere
/*
remote_state {
	backend = "s3"
  	config = {
		skip_requesting_account_id = true
		skip_credentials_validation = true
        skip_get_ec2_platforms = true
        skip_metadata_api_check = true
#        access_key = local.access_key
#        secret_key = local.secret_key
        endpoint = local.spaces_endpoint
        region = "us-east-1"
        bucket = local.spaces_bucket // name of your space
        key = "${path_relative_to_include()}.terraform.tfstate"
  }
  generate = {
    path      = "backend.tf"
    if_exists = "overwrite_terragrunt"
  }
}
*/

generate "s3_backend" {
  path      = "s3_backend.tf"
  if_exists = "overwrite_terragrunt"
  contents = <<EOF
terraform {
  backend "s3" {
    access_key                 = "${local.access_key}"
    bucket                     = "${local.spaces_bucket}"
    # endpoint should match bucket's region
    endpoint                    = "${local.spaces_endpoint}"
    key                         = "rke2/${path_relative_to_include()}/terraform.tfstate"
    # it may be any valid aws region, it doesn't affect anything
    region                      = "us-west-1"
    secret_key                  = "${local.secret_key}"
    skip_credentials_validation = true
    skip_metadata_api_check     = true
  }
}
EOF
}

inputs = {
	do_token = local.token
	domain   = local.creds.base_domain
}