terraform {
	required_providers {
    	digitalocean = {
      		source = "digitalocean/digitalocean"
      		version = "~> 2.0"
    	}
    	local = {
      		source = "hashicorp/local"
      		version = "2.1.0"
    	}
    	null = {
      		source = "hashicorp/null"
      		version = "3.1.0"
    	}
    	random = {
      		source = "hashicorp/random"
      		version = "3.1.0"
    	}
    	tls = {
      		source = "hashicorp/tls"
      		version = "3.1.0"
    	}
  	}
}

provider "digitalocean" {
	token = var.do_token
}

provider "local" {
	# Configuration options
}

provider "null" {
  # Configuration options
}

provider "tls" {
	# Configuration options
}