variable domain {
    description = "Domain for the cluster"
    type        = string
}

variable do_token {
    description = "Digital Ocean API token"
    type        = string
}

variable initialize {
    description = "Flag to do first time setup"
    type        = bool
    default     = false
}