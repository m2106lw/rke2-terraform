write-kubeconfig-mode: "0644"
tls-san:
- ${cluster_url}
node-label:
- "test=yes"
# Improve this token somehow
token: my-shared-secret
server: https://${cluster_url}:9345