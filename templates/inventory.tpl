[rke2]
%{ for vm in vms ~}
${vm.name} ansible_host=${ips[vm.name].ip_address}
%{ endfor ~}

[rke2:vars]
ansible_user=root
ansible_private_key_file=${sshkey}
ansible_host_key_checking=no